﻿# Prompt user for IP address
$ipAddress = Read-Host -Prompt "Enter the desired IP address (e.g., 192.168.1.100)"

# Validate the entered IP address
if (-not ($ipAddress -as [System.Net.IPAddress])) {
    Write-Host "Invalid IP address. Please enter a valid IPv4 address."
    exit
}

# Calculate subnet mask based on the first octet of the entered IP address
$subnetMask = "255.255.255.0"  # Default subnet mask for Class C networks

# Check the first octet to determine the class of the network and set the appropriate subnet mask
$firstOctet = $ipAddress.Split('.')[0] -as [int]
if ($firstOctet -lt 128) {
    $subnetMask = "255.0.0.0"  # Class A
} elseif ($firstOctet -lt 192) {
    $subnetMask = "255.255.0.0"  # Class B
}

# Get the network interface name (Ethernet adapter) - you can modify this based on your system
$networkInterface = "Ethernet"

# Set the static IP address and subnet mask
$networkInterfaceObj = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object { $_.Description -like "*$networkInterface*" }
$networkInterfaceObj.EnableStatic($ipAddress, $subnetMask)

Write-Host "Static IP address set to $ipAddress with subnet mask $subnetMask for $networkInterface."